from fabric.api import *


@task(alias='2pypi')
def distribute_to_pypi():
    """
    Distribute the eggplant to PyPI
    """
    local('python setup.py sdist upload')
    local('rm -rf ground_soil.egg-info')
    local('rm -rf dist')

