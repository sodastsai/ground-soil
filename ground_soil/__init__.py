#
# Ground Soil
# __init__.py
#
# Copyright (c) 2013. Dian-Je Tsai. All rights reserved.
# Author: sodas tsai
#

from __future__ import unicode_literals

__version__ = '0.2.5'
VERSION = tuple(map(lambda x: int(x), __version__.split('.')))
